// This file is part of the Acts project.
//
// Copyright (C) 2017-2018 CERN for the benefit of the Acts project
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#include "ACTFW/Plugins/Geant4/MMSteppingAction.hpp"
#include <stdexcept>
#include "Acts/Utilities/Units.hpp"
#include "G4Material.hh"
#include "G4Step.hh"

FW::Geant4::MMSteppingAction* FW::Geant4::MMSteppingAction::fgInstance
    = nullptr;

FW::Geant4::MMSteppingAction*
FW::Geant4::MMSteppingAction::Instance()
{
  // Static acces function via G4RunManager
  return fgInstance;
}

FW::Geant4::MMSteppingAction::MMSteppingAction()
  : G4UserSteppingAction(), m_steps()
// m_volMgr(MaterialRunAction::Instance()->getGeant4VolumeManager())
{
  if (fgInstance) {
    throw std::logic_error("Attempted to duplicate a singleton");
  } else {
    fgInstance = this;
  }
}

FW::Geant4::MMSteppingAction::~MMSteppingAction()
{
  fgInstance = nullptr;
}

void
FW::Geant4::MMSteppingAction::UserSteppingAction(const G4Step* step)
{
  // get the material
  G4Material* material = step->GetPreStepPoint()->GetMaterial();

  if (material && material->GetName() != "Vacuum"
      && material->GetName() != "Air") {
    // go through the elements of the material & weigh it with its fraction
    const G4ElementVector* elements  = material->GetElementVector();
    const G4double*        fraction  = material->GetFractionVector();
    size_t                 nElements = material->GetNumberOfElements();
    double                 A         = 0.;
    double                 Z         = 0.;
    double                 X0        = material->GetRadlen();
    double                 L0        = material->GetNuclearInterLength();
    double rho        = material->GetDensity() * CLHEP::mm3 / CLHEP::gram;
    double steplength = step->GetStepLength() / CLHEP::mm;
    double nonIonEDep = step->GetNonIonizingEnergyDeposit();
    double totEDep    = step->GetTotalEnergyDeposit();
    // double EPre = step->GetPreStepPoint()->GetKineticEnergy();
    // double EPost = step->GetPostStepPoint()->GetKineticEnergy();
    
    
    if (nElements == 1) {
      A = material->GetA() * CLHEP::mole / CLHEP::gram;
      Z = material->GetZ();
    } else {
      for (size_t i = 0; i < nElements; i++) {
        A += elements->at(i)->GetA() * fraction[i] * CLHEP::mole / CLHEP::gram;
        Z += elements->at(i)->GetZ() * fraction[i];
      }
    }

    
    /*   G4cout << *material << G4endl;
       G4cout << "----G4StepMaterial----" << G4endl;
       /// @TODO remove output after testing
       G4cout << "Material: " << material->GetName() << G4endl;
       G4cout << "X0: " << X0 << G4endl;
       G4cout << "L0: " << L0 << G4endl;
       G4cout << "A: " << A << G4endl;
       G4cout << "Z: " << Z << G4endl;
       G4cout << "rho: " << rho << G4endl;
       G4cout << "steplength: " << steplength << G4endl;
       G4cout << "non ionizing e deposit: " << nonIonEDep << G4endl;                                             
       G4cout << "total e deposit: " << totEDep << G4endl;
       G4cout << "e pre - post: " << EPre-EPost << G4endl; */

    // create the track info
    G4Track* track      = step->GetTrack(); 
    const auto& trkPos  = track->GetPosition();
    const auto& par     = track->GetDynamicParticle();
    double parKinEnergy = par->GetKineticEnergy();
    G4LorentzVector vpar = par->Get4Momentum();
    int parPDG          = par->GetPDGcode();
    double parMass      = par->GetMass();
    std::string parName = track->GetDefinition()->GetParticleName();
    
    // create the RecordedMaterialProperties

    const auto&               rawPos = step->GetPreStepPoint()->GetPosition();
    Acts::MaterialInteraction mInteraction;
    mInteraction.position = Acts::Vector3D(rawPos.x(), rawPos.y(), rawPos.z());
    mInteraction.materialProperties
        = Acts::MaterialProperties(X0, L0, A, Z, rho, steplength);
    mInteraction.nonIonEnergyDeposit = nonIonEDep;
    mInteraction.totEnergyDeposit = totEDep;


    if(track->GetTrackID() == 1){
      mInteraction.parPDG      = parPDG;
      mInteraction.parPosition = Acts::Vector3D(trkPos.x(), trkPos.y(), trkPos.z());
      mInteraction.parEnergy   = vpar.e();//parKinEnergy;
      mInteraction.parPx       = vpar.px();
      mInteraction.parPy       = vpar.py();
      mInteraction.parPz       = vpar.pz();
      mInteraction.parMass     = parMass; 
      m_steps.push_back(mInteraction);
      /*
      G4cout << "--------- Step " << m_steps.size() <<" --------" << G4endl;
      G4cout << "particle type   : " << parName << G4endl;
      G4cout << "particle pdg    : " << parPDG << G4endl;
      G4cout << "track ID        : " << track->GetTrackID() << G4endl;
      G4cout << "particle energy : " << parKinEnergy << G4endl;
      G4cout << "particle pos x  : " << trkPos.x() << "\t y: " << trkPos.y() << G4endl;
      G4cout << "particle px     : " << vpar.px() << "\t py: " << vpar.py() << "\t pz: " << vpar.pz() << "\t e: " << vpar.e() << G4endl;
      G4cout << "particle theta  : " << vpar.theta() << G4endl;
      */
    }

  }
}

void
FW::Geant4::MMSteppingAction::Reset()
{
  m_steps.clear();
}
